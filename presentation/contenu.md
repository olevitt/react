<!-- .slide: class="slide" -->
# De JQuery à React

---

<!-- .slide: class="slide" -->
# Situation actuelle

Application Migrations

----

## Architecture  
* Client HTML5 + JQuery  
* Backend webservice PHP (Symfony) 
* Base de données sqlserver / mysql  

=> Architecture moderne 

---

<!-- .slide: class="slide" -->
# Limites de l'existant

----

* Gestion des dépendances  
* Séparation build time / runtime  
* Tests automatisés  
* Réutilisabilité des composants

---


<!-- .slide: class="slide" -->
# Propositions de solutions

----

* Gestion des dépendances : NPM
* Séparation build time / runtime : NodeJS  
* Tests automatisés : NodeJS  
* Réutilisabilité des composants : React  

---


<!-- .slide: class="slide" -->
# Le trio gagnant

----

## NodeJS

* Javascript en dehors du navigateur  
* Standalone  
* Idée simple ... mais révolutionnaire !  

----

## NPM

* Node Package Manager
* Inclu dans NodeJS
* Gestion des dépendances / build
* Fichier `package.json`  
* Equivalent PHP : `Composer`  

----

## React

* Bibliothèque de création de composants graphiques  
* Centré sur la réutilisabilité  
* Alternatives : Angular, VueJS  

---


<!-- .slide: class="slide" -->
# Structure de projet  

----

## Le fichier central : package.json
```json
{
	"name": "onyxia",
	"version": "0.1.0",
	"homepage": "/",
	"dependencies": {
		"react": "^16.6.0"
	},
	"devDependencies": {
		"cross-env": "^5.2.0"
	},
	"scripts": {
		"start": "npm-run-all -p watch-css start-js"
	}
}
```  

----

## Le mode facile : create-react-app  

```
npx create-react-app my-app
```

----

## Le résultat
```
my-app
├── README.md
├── node_modules
├── package.json
├── .gitignore
├── public
│   ├── favicon.ico
│   ├── index.html
│   └── manifest.json
└── src
	├── App.css
	├── App.js
	├── App.test.js
	├── index.css
	├── index.js
	├── logo.svg
	└── serviceWorker.js
```

----

## Lancer l'application
```
npm run start
```
```
npm run test
```
```
npm run build
```

---

# Let's go
![](images/chat.jpg) <!-- .element height="30%" width="30%" -->

----

## Choix de l'IDE
* IDE du moment : Visual Studio Code 
https://hackernoon.com/the-rise-of-microsoft-visual-studio-code-a3d143490a52  

----

## Installation de NodeJS
* https://nodejs.org/en/

---

# React  
[https://reactjs.org/](https://reactjs.org)

----

## C'est quoi ?

* Une bibliothèque pour écrire des composants graphiques  
* Pas un framework !  


----


## Parlons du DOM

* Document Object Model
* En gros : le HTML vivant  
* JS : manipuler le DOM  


----


## Le DOM : sans react  

```HTML
<div id="contenu"></div>

<script>
document.getElementById('contenu').innerHTML = "<b>Hello world</b>";
</script>
```

----

## React & le DOM

* On confie TOUTE la gestion du DOM à React  
* On confie TOUTE la gestion du DOM à React
* On confie TOUTE la gestion du DOM à React
* On confie TOUTE la gestion du DOM à React
* On confie TOUTE la gestion du DOM à React  

----

## React & le DOM : concrètement

index.html  
```HTML
<div id="root"></div>
```  

index.js  
```Javascript
ReactDOM.render(<App />, document.getElementById('root'));
```  

----

## Le concept  

Créer des composants réutilisables
```Javascript
class MaListe extends React.Component {
	render() {
	  return (
		<div classname="list">
		  <h1>Ma liste</h1>
		  <table>
			<li>Element 1</li>
			<li>Element 2</li>
			<li>Element 3</li>
		  </table>
		</div>
	  );
	}
  }
```  

---  

# React, concepts

----  

## Les props  

```HTML

```